<?php

class CPPWrapperConfig
{
    /** @var  string */
    public $url_piste_get_token;
    /** @var  string */
    public $client_id;
    /** @var  string */
    public $client_secret;
    /** @var  string */
    public $url_piste_api;
    /** @var  string */
    public $cpro_account;

    public $proxy;

    public $user_login;
    public $user_password;

    /** @var  string */
    public $user_role;

    public $identifiant_structure_cpp;
    public $service_destinataire;

    /** @var ?bool $fetchDownloadedInvoices */
    public $fetchDownloadedInvoices;
}
