<?php

declare(strict_types=1);

namespace Mailsec\Exception;

final class UnavailableMailException extends \Exception
{
}
